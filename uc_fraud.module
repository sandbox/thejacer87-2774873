<?php

/**
 * @file
 * uc_fraud main file.
 */

/**
 * Implements hook_menu().
 */
function uc_fraud_menu() {
  $items['admin/store/settings/fraud'] = array(
    'title' => 'Fraud Score Settings',
    'description' => 'Manage the caps for the Fraud Score.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_fraud_admin_settings'),
    'access arguments' => array('administer frauds'),
    'file' => '/includes/uc_fraud.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function uc_fraud_permission() {
  return array(
    'administer frauds' => array(
      'title' => t('Administer Fraud Caps'),
      'description' => t('Allows users to configure the caps defining frauds.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_help().
 */
function uc_fraud_help($path) {
  switch ($path) {
    case 'admin/store/settings/fraud':
      return '<p>' . t('This page lets you manage the caps for the Fraud Score.') . '</p>';
  }
}

/**
 * Implements hook_views_api().
 */
function uc_fraud_views_api() {
  return array(
    'api' => '3.0',
    'path' => drupal_get_path('module', 'uc_fraud') . '/includes/views',
  );
}

function uc_fraud_increase_fraud_count($order, $counter) {
  $order_id = $order->order_id;
  $fraud_score = uc_fraud_get_fraud_score_for_order_id($order_id);
  $fraud_score += $counter;
  uc_fraud_write_record(array(
    'order_id' => $order->order_id,
    'fraud_score' => $fraud_score,
  ));
  rules_invoke_event('uc_fraud_count_changed', $order);
}

function uc_fraud_decrease_fraud_count($order, $counter) {
  $order_id = $order->order_id;
  $fraud_score = uc_fraud_get_fraud_score_for_order_id($order_id);
  $fraud_score -= $counter;
  uc_fraud_write_record(array(
    'order_id' => $order->order_id,
    'fraud_score' => $fraud_score,
  ));
  rules_invoke_event('uc_fraud_count_changed', $order);
}

function uc_fraud_reset_fraud_count($order) {
  uc_fraud_write_record(array(
    'order_id' => $order->order_id,
    'fraud_score' => 0,
  ));
  rules_invoke_event('uc_fraud_count_changed', $order);
}

function uc_fraud_is_whitelisted($order) {
  if ($fraud_score = uc_fraud_get_fraud_score_for_order_id($order->order_id)) {
    return $fraud_score < variable_get('uc_fraud_greylist_lower_cap', 10);
  }
  // Return true if no fraud score on the order.
  return TRUE;
}

function uc_fraud_is_greylisted($order) {
  if ($fraud_score = uc_fraud_get_fraud_score_for_order_id($order->order_id)) {
    $greylist_lower_cap = variable_get('uc_fraud_greylist_lower_cap', 10);
    $greylist_upper_cap = variable_get('uc_fraud_blacklist_lower_cap', 20);
    return $fraud_score >= $greylist_lower_cap && $fraud_score < $greylist_upper_cap;
  }
  // Return false if no fraud score on the order.
  return FALSE;
}

function uc_fraud_is_blacklisted($order) {
  if ($fraud_score = uc_fraud_get_fraud_score_for_order_id($order->order_id)) {
    return $fraud_score >= variable_get('uc_fraud_blacklist_cap', 20);
  }
  // Return false if no fraud score on the order.
  return FALSE;
}

/**
 * Checks the shipping street address for a PO Box.
 *
 * @param $order_wrapper
 * @return bool
 */
function uc_order_has_po_box($order) {
  $street1_match = preg_match("/(p(.*)?o(.*?)?)(\s+)?(bo?x)?(\s+)?[#]?(\d+)?/i", $order->delivery_street1);
  $street2_match = preg_match("/(p(.*)?o(.*?)?)(\s+)?(bo?x)?(\s+)?[#]?(\d+)?/i", $order->delivery_street2);
  return $street1_match || $street2_match;
}

function uc_fraud_time_since_previous_order($order, $time_elapsed, $order_status = 'completed') {
  // Look for {users} orders created within {$time_elapsed} minutes
  $query = db_select('uc_orders', 'u')
    ->fields('u', array('order_id'))
    ->condition('uid', $order->uid)
    ->condition('order_status', $order_status)
    ->condition('created', time() - ($time_elapsed * 60), '>=');
  return !empty($query->execute()->fetchAssoc());
}

/**
 * Updates or inserts fraud_score for order id.
 *
 * @param array $record
 *   Must be an array with two keys: order_id and fraud_score
 */
function uc_fraud_write_record(array $record) {
  db_merge('uc_fraud_fraud_score')
    ->key(array('order_id' => $record['order_id']))
    ->fields($record)
    ->execute();
}

/**
 * Returns the current fraud score for order id.
 *
 * @param $order_id
 *
 * @return int
 */
function uc_fraud_get_fraud_score_for_order_id($order_id) {
  return db_select('uc_fraud_fraud_score', 'f')
    ->fields('f', array('fraud_score'))
    ->condition('order_id', $order_id)
    ->execute()
    ->fetchField();
}

function uc_fraud_condition_value_operator_options() {
  return array(
    '<' => '<',
    '<=' => '<=',
    '=' => '=',
    '>=' => '>=',
    '>' => '>',
  );
}

function uc_order_condition_fraud_level_operator_options() {
  return array(
    'uc_fraud_is_whitelisted' => 'Whitelisted',
    'uc_fraud_is_greylisted' => 'Greylisted',
    'uc_fraud_is_blacklisted' => 'Blacklisted',
  );
}


