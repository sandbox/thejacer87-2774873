<?php

/**
 * Implements hook_views_default_views_alter().
 */
function uc_fraud_views_default_views_alter(&$views) {
  if (array_key_exists('uc_orders', $views)) {
    // Add Ubercart Fraud Level to Orders View
    $views['uc_orders']->display['default']->display_options['fields']['uc_fraud_level']['id'] = 'uc_fraud_level';
    $views['uc_orders']->display['default']->display_options['fields']['uc_fraud_level']['table'] = 'uc_fraud';
    $views['uc_orders']->display['default']->display_options['fields']['uc_fraud_level']['field'] = 'uc_fraud_level';
  }
}