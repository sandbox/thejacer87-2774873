<?php

/**
 * @file
 * Custom views handler definition.
 */

/**
 * Custom handler class.
 * @group views_field_handlers
 */
class views_handler_uc_fraud_level extends views_handler_field {

  /**
   * {@inheritdoc}
   */
  function query() {
  }

  /**
   * {@inheritdoc}
   */
  function render($values) {
    $order = uc_order_load($values->order_id);
    if (uc_fraud_is_whitelisted($order)) {
      return t('Whitelisted');
    }
    elseif (uc_fraud_is_greylisted($order)) {
      return t('Greylisted');
    }
    elseif (uc_fraud_is_blacklisted($order)) {
      return t('Blacklisted');
    }
  }

}
