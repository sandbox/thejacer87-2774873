<?php

/**
 * Implements hook_views_data().
 */
function uc_fraud_views_data() {
  $data['uc_fraud']['table']['group'] = t('Ubercart Fraud');
  $data['uc_fraud']['table']['join'] = array(
    // Exist in all views.
    '#global' => array(),
  );

  $data['uc_fraud']['uc_fraud_level'] = array(
    'title' => t('Ubercart Fraud Level'),
    'help' => t('Shows the fraud level.'),
    'field' => array(
      'handler' => 'views_handler_uc_fraud_level',
      'click sortable' => TRUE,
    ),
  );

  return $data;
}
