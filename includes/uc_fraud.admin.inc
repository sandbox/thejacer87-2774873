<?php

/**
 * @file
 * The admin settings form.
 */

/**
 * Administration form for uc_fraud.
 */
function uc_fraud_admin_settings() {
  $form = array();

  /*=========Caps========*/
  $form['uc_fraud_caps'] = array(
    '#type' => 'fieldset',
    '#title' => t('Commerce Fraud Caps Settings'),
  );

  $form['uc_fraud_caps']['uc_fraud_greylist_cap'] = array(
    '#type' => 'textfield',
    '#title' => t('Greylist lower cap'),
    '#description' => t('The lower cap for an order to be considered greylisted.'),
    '#default_value' => variable_get('uc_fraud_greylist_lower_cap', 10),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['uc_fraud_caps']['uc_fraud_blacklist_cap'] = array(
    '#type' => 'textfield',
    '#title' => t('Blacklist lower cap'),
    '#description' => t('The lower cap for an order to be considered blacklisted.'),
    '#default_value' => variable_get('uc_fraud_blacklist_lower_cap', 20),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  return system_settings_form($form);
}
