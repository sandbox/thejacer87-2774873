<?php

/**
 * @file
 * Defines the default rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uc_fraud_default_rules_configuration() {
  $rules = array();

  $rule = rules_reaction_rule();
  $rule->label = t('Change order status depending on Fraud level');
  $rule->tags = array('Ubercart Fraud');
  $rule->weight = 100;
  $rule->active = TRUE;
  $rule
    ->event('uc_checkout_complete')
    ->condition('uc_fraud_compare_fraud_level', array(
      'uc_order:select' => 'order',
      'fraud_level_function' => 'uc_fraud_is_blacklisted',
    ))
    ->action('uc_order_update_status', array(
      'order:select' => 'order',
      'order_status' => 'uc_order_status_options_list',
    ));
  $rules['uc_fraud_rules_set_fraudulent'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Add Fraud Score to Orders with PO Box');
  $rule->tags = array('Ubercart Fraud');
  $rule
    ->event('uc_checkout_complete')
    ->condition('uc_fraud_has_po_box', array(
      'uc_order:select' => 'order',
    ))
    ->action('uc_fraud_increase_fraud_count', array(
      'uc_order:select' => 'order',
      'counter' => 10,
    ));
  $rules['uc_fraud_rules_default_po_box'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Add Fraud Score to expensive Orders');
  $rule->tags = array('Ubercart Fraud');
  $rule->active = true;
  $rule
    ->event('uc_checkout_complete')
    ->condition('barcodestalk_labels_condition_order_total', array(
      'order:select' => 'order',
      'total_value' => 800,
      'order_total_comparison' => 'greater',
    ))
    ->action('uc_fraud_increase_fraud_count', array(
      'uc_order:select' => 'order',
      'counter' => 25,
    ));
  $rules['uc_fraud_rules_default_order_total'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Add Fraud Score to Order placed within X minutes of previous Order');
  $rule->tags = array('Ubercart Fraud');
  $rule
    ->event('uc_checkout_complete')
    ->condition('barcodestalk_labels_condition_order_total', array(
      'order:select' => 'order',
      'total_value' => 800,
      'order_total_comparison' => 'greater',
    ))
    ->action('uc_fraud_increase_fraud_count', array(
      'uc_order:select' => 'order',
      'counter' => 5,
    ));
  $rules['uc_fraud_rules_default_time_since_previous_order'] = $rule;

  return $rules;
}

/**
 * Implements hook_default_rules_configuration_alter().
 */
function uc_fraud_default_rules_configuration_alter(&$configs) {
  $configs['uc_checkout_complete_paid']->condition(rules_condition('uc_fraud_is_blacklisted', array(
    'uc_order:select' => 'order',
  ))->negate());
}
