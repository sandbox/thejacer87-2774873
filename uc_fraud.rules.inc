<?php

/**
 * @file
 * uc_fraud custom rules.
 */

/**
 * Implements hook_rules_event_info().
 */
function uc_fraud_rules_event_info() {
  $events = array();

  $events['uc_fraud_count_changed'] = array(
    'label' => t('Fraud count changed'),
    'group' => t('Ubercart Fraud'),
    'variables' => array(
      'uc_order' => array(
        'label' => t('The Ubercart order'),
        'type' => 'uc_order',
      ),
    ),
  );

  return $events;
}

/**
 * Implements hook_rules_condition_info().
 */
function uc_fraud_rules_condition_info() {
  $conditions = array();

  $conditions['uc_fraud_is_whitelisted'] = array(
    'label' => t('Order is whitelisted'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'uc_order' => array(
        'label' => t('The ubercart order'),
        'type' => 'uc_order',
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_fraud_rules_is_whitelisted',
    ),
  );

  $conditions['uc_fraud_is_greylisted'] = array(
    'label' => t('Order is greylisted'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'uc_order' => array(
        'label' => t('The ubercart order'),
        'type' => 'uc_order',
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_fraud_rules_is_greylisted',
    ),
  );

  $conditions['uc_fraud_is_blacklisted'] = array(
    'label' => t('Order is blacklisted'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'uc_order' => array(
        'label' => t('The ubercart order'),
        'type' => 'uc_order',
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_fraud_rules_is_blacklisted',
    ),
  );

  $conditions['uc_fraud_has_po_box'] = array(
    'label' => t('Order Shipping Address is PO Box'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'uc_order' => array(
        'label' => t('The ubercart order'),
        'type' => 'uc_order',
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_order_rules_has_po_box',
    ),
  );

  $conditions['uc_fraud_time_since_previous_order'] = array(
    'label' => t('Check how long since last order.'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'uc_order' => array(
        'label' => t('The ubercart order'),
        'type' => 'uc_order',
      ),
      'time_elapsed' => array(
        'type' => 'integer',
        'label' => t('Minutes since last order'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_fraud_rules_time_since_previous_order',
    ),
  );

  $conditions['uc_fraud_compare_fraud_level'] = array(
    'label' => t('Compare an order\'s fraud level'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order'),
      ),
      'fraud_level_function' => array(
        'type' => 'text',
        'label' => t('Operator'),
        'options list' => 'uc_order_condition_fraud_level_operator_options',
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_fraud_rules_compare_fraud_level',
    ),
  );

  $conditions['uc_fraud_compare_order_total'] = array(
    'label' => t('Compare an order\'s subtotal'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order'),
      ),
      'total_value' => array(
        'type' => 'decimal',
        'label' => t('Maximum Order Total Value'),
      ),
      'order_total_comparison' => array(
        'type' => 'text',
        'label' => t('Operator'),
        'options list' => 'uc_order_condition_value_operator_options',
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_fraud_rules_compare_order_total',
    ),
  );

  return $conditions;
}

/**
 * Whitelist condition callback.
 */
function uc_fraud_rules_is_whitelisted($order) {
  return uc_fraud_is_whitelisted($order);
}

/**
 * Greylist condition callback.
 */
function uc_fraud_rules_is_greylisted($order) {
  return uc_fraud_is_greylisted($order);
}

/**
 * Blacklist condition callback.
 */
function uc_fraud_rules_is_blacklisted($order) {
  return uc_fraud_is_blacklisted($order);
}

/**
 * PO Box condition callback.
 */
function uc_order_rules_has_po_box($order) {
  return uc_order_has_po_box($order);
}

/**
 * Time since previous Order callback.
 */
function uc_fraud_rules_time_since_previous_order($order, $time_elapsed) {
  return uc_fraud_time_since_previous_order($order, $time_elapsed);
}

function uc_fraud_rules_compare_fraud_level($order, $fraud_level_check) {
  return $fraud_level_check($order);
}

function uc_fraud_rules_compare_order_total($order, $subtotal, $op) {
  $order_subtotal = uc_order_get_total($order, TRUE);

  switch ($op) {
    case 'less':
      return $order_subtotal < $subtotal;
    case 'less_equal':
      return $order_subtotal <= $subtotal;
    case 'equal':
      return $order_subtotal == $subtotal;
    case 'greater_equal':
      return $order_subtotal >= $subtotal;
    case 'greater':
      return $order_subtotal > $subtotal;
  }
}

/**
 * Implements hook_rules_action_info().
 */
function uc_fraud_rules_action_info() {
  $actions = array();

  $actions['uc_fraud_increase_fraud_count'] = array(
    'label' => t('Increase the fraud count'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'uc_order' => array(
        'type' => 'uc_order',
        'label' => t('The ubercart order'),
      ),
      'counter' => array(
        'type' => 'integer',
        'label' => t('The counter'),
        'description' => t('Defines by how much the fraud count is increased.'),
        'default value' => 1,
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_fraud_rules_increase_fraud_count',
    ),
    'access callback' => 'uc_order_rules_access',
  );

  $actions['uc_fraud_decrease_fraud_count'] = array(
    'label' => t('Decrease the fraud count'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'uc_order' => array(
        'type' => 'uc_order',
        'label' => t('The ubercart order'),
      ),
      'counter' => array(
        'type' => 'integer',
        'label' => t('The counter'),
        'description' => t('Defines by how much the fraud count is increased.'),
        'default value' => 1,
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_fraud_rules_decrease_fraud_count',
    ),
    'access callback' => 'uc_order_rules_access',
  );

  $actions['uc_fraud_reset_fraud_count'] = array(
    'label' => t('Reset the fraud count'),
    'group' => t('Ubercart Fraud'),
    'parameter' => array(
      'uc_order' => array(
        'type' => 'uc_order',
        'label' => t('The ubercart order'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'uc_fraud_rules_reset_fraud_count',
    ),
    'access callback' => 'uc_order_rules_access',
  );

  return $actions;
}

/**
 * Implements the callback of the "Increase the fraud count" rule.
 *
 * @param $order
 *   The current uc_order.
 * @param integer $counter
 *   By how much to increase the fraud count.
 */
function uc_fraud_rules_increase_fraud_count($order, $counter) {
  uc_fraud_increase_fraud_count($order, $counter);
}

/**
 * Implements the callback of the "Decrease the fraud count" rule.
 *
 * @param $order
 *   The current uc_order.
 * @param integer $counter
 *   By how much to decrease the fraud count.
 */
function uc_fraud_rules_decrease_fraud_count($order, $counter) {
  uc_fraud_decrease_fraud_count($order, $counter);
}

/**
 * Implements the callback of "Reset the fraud count" rule.
 *
 * @param $order
 *   The current uc_order.
 */
function uc_fraud_rules_reset_fraud_count($order) {
  uc_fraud_reset_fraud_count($order);
  rules_invoke_event('uc_fraud_count_changed', $order);
}
